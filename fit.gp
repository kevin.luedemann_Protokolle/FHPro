reset
set term epslatex color solid
set output 'plot1.tex'
set xrange [0:60]
set yrange [0:9000]
f(x)=a*x+b
g(x)=c*x+d
h(x)=e*x+f
i(x)=g*x+h
j(x)=i*x+j
k(x)=k*x+l
set key left
fit [6:15.9] f(x) 'daten.dat' u 1:2:(1) via a,b
fit [17:20] g(x) 'daten.dat' u 1:2:(1) via c,d
fit [21:32] h(x) 'daten.dat' u 1:2:(1) via e,f
fit [33.4:37.35] i(x) 'daten.dat' u 1:2:(1) via g,h
fit [39.5:49] j(x) 'daten.dat' u 1:2:(1) via i,j
fit [50.5:53] k(x) 'daten.dat' u 1:2:(1) via k,l
p 'daten.dat' u 1:2:(1) w ye t'Messwerte', f(x),g(x),h(x),i(x),j(x),k(x)
set output

